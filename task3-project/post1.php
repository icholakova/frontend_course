<?php
	$page_title = 'Current post';
	$active = array("","","active","");
	include('templates/header.php');
?>
<section class="current_post_wrapper">
<article>
	<h1>Sticker Mule. Best Place For Your Sticker Needs!</h1>
	<div class="clearfix">
		<div class="left_side float_left">
			<figure>
				<img src="img/post_1_img_1.jpg" alt="">
			</figure>
			<figure>
				<img src="img/post_1_img_2.jpg" alt="">
			</figure>
		</div>
		<div class="right_side float_right">
			<table>
				<thead>
					<tr>
						<th>Date</th>
						<th>Tags</th>
						<th>Author</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>April 15, 2012</td>
						<td>Website | Design</td>
						<td>Michael Reimer</td>
					</tr>
				</tbody>
			</table>
			<p>
				Lorem ipsum dolor sit amet, mollis epicuri pri ei, perpetua honestatis ad vix. Ne duo ludus putent, cu causae tamquam voluptua duo. Agam officiis no duo, ut reque decore sea. Cu eripuit accusam vix. Facete blandit detraxit pri cu, sea soluta doming civibus ea.
				<br><br>
				Eum eu tale clita iuvaret, cu est saperet forensibus interesset, cum ne case iusto oportere. Id idque indoctum eum, menandri mediocrem has ei. At usu modo quaerendum. Sit ei dicunt tacimates, mea ea enim eirmod suscipiantur, amet dicit ancillae vel in. Ex mea augue eloquentiam, his postea dolorem suavitate ea. Mel hendrerit accommodare concludaturque ex.
			</p>
			<a class="link_button" href="javascript:void(0)" target="_blank">Visit website</a>
		</div>
	</div>
</article>
<section class="similar_posts">
	<h1>Similar Posts. Check Them Out!</h1>
	<div class="clearfix">
		<article class="post">
			<a href="javascript:void(0)" class="post_thumb">
				<div class="post_thumb_hover">
					<div class="post_hover_info clearfix">
						<div class="post_views">857</div>
						<div class="post_date">07/05/12</div>
						<div class="post_likes">588</div>
					</div>
				</div>
				<div class="thumb_wrapper">
					<img src="img/post_thumb_6.jpg" alt="Character Design" />
				</div>
			</a>
			<div class="post_body">
				<h2 class="post_title">
					<a href="javascript:void(0)">Character Design</a>
				</h2>
				<ul class="post_meta clearfix">
					<li>June 15, 2012</li>
				</ul>
			</div>
		</article>
		<article class="post">
			<a href="javascript:void(0)" class="post_thumb">
				<div class="post_thumb_hover">
					<div class="post_hover_info clearfix">
						<div class="post_views">857</div>
						<div class="post_date">07/05/12</div>
						<div class="post_likes">588</div>
					</div>
				</div>
				<div class="thumb_wrapper">
					<img src="img/sp_img_2.jpg" alt="Top iPhone Apps" />
				</div>
			</a>
			<div class="post_body">
				<h2 class="post_title">
					<a href="javascript:void(0)">Top iPhone Apps</a>
				</h2>
				<ul class="post_meta clearfix">
					<li>June 15, 2012</li>
				</ul>
			</div>
		</article>
		<article class="post">
			<a href="javascript:void(0)" class="post_thumb">
				<div class="post_thumb_hover">
					<div class="post_hover_info clearfix">
						<div class="post_views">857</div>
						<div class="post_date">07/05/12</div>
						<div class="post_likes">588</div>
					</div>
				</div>
				<div class="thumb_wrapper">
					<img src="img/sp_img_3.jpg" alt="Social Media Buttons" />
				</div>
			</a>
			<div class="post_body">
				<h2 class="post_title">
					<a href="javascript:void(0)">Social Media Buttons</a>
				</h2>
				<ul class="post_meta clearfix">
					<li>June 15, 2012</li>
				</ul>
			</div>
		</article>
		<article class="post clear_margin_right">
			<a href="javascript:void(0)" class="post_thumb">
				<div class="post_thumb_hover">
					<div class="post_hover_info clearfix">
						<div class="post_views">857</div>
						<div class="post_date">07/05/12</div>
						<div class="post_likes">588</div>
					</div>
				</div>
				<div class="thumb_wrapper">
					<img src="img/sp_img_4.jpg" alt="10 Amazing Websites" />
				</div>
			</a>
			<div class="post_body">
				<h2 class="post_title">
					<a href="javascript:void(0)">10 Amazing Websites</a>
				</h2>
				<ul class="post_meta clearfix">
					<li>June 15, 2012</li>
				</ul>
			</div>
		</article>
	</div>
</section>
<?php include('templates/footer.php') ?>