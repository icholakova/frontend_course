<?php
    $page_title = 'Contacts';
    $active = array("","","","active");
    include('templates/header.php');
?>
<section id="contacts_wrapper">
	<h1>Contact Us. Locate Us. Send Us Your Thoughts!</h1>
	<p>
		Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. 
		Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet. 
		Duis sagittis ipsum. Praesent mauris. Maecenas in magna mollis lectus lacinia mollis. 
	</p>
	<div id="map-canvas" style="height: 400px; width: 939px;"></div>
	<form id="contact_form" action="">
		<input type="text" placeholder="FIRST NAME">
		<input type="text" placeholder="LAST NAME">
		<input type="email" placeholder="EMAIL ADDRESS">
		<textarea placeholder="MESSAGE"></textarea>
		<input type="submit" value="Submit">
	</form>
</section>
<?php include('templates/footer.php') ?>