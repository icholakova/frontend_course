<?php include '/inc/env.php'; ?>
<!DOCTYPE html>
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="no-js ie8 lt-ie9" lang="en"> <![endif]-->
<!--[if IE 9]> <html class="no-js eq-ie9 ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <base href="<?php echo $basehref; ?>">
    <title><?php echo $page_title; ?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/styles.css">
    <?php if($page_title == 'Contacts') { ?>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCXSvNhrGGECmJMZKs3Ewbz32jVRvLTTq4&language=en"></script>
    <?php } ?>
    <script type="text/javascript">
        var currentPage = '<?php echo $page_title; ?>';
    </script>
</head>
<body>
    <div class="wrapper clearfix" role="main">
        <header id="header_top">
            <div class="top_bar clearfix">
                <a href="index.php" class="logo float_left" title="Back to home">
                    <img src="img/logo.png" width="164" height="28px" alt="Gridzilla">
                </a>
                <div class="search_wrapper float_right">
                    <form id="search_form" action="">
                        <input type="search" placeholder="Enter your search...">
                        <input type="submit" title="Search" value="&nbsp" alt="Search">
                    </form>
                </div>
            </div>
            <nav class="main_nav">
                <ul class="clearfix">
                    <li class="<?php echo $active[0];?>"><a href="index.php">Home</a></li>
                    <li class="<?php echo $active[1];?>"><a href="about.php">About</a></li>
                    <li class="<?php echo $active[2];?>"><a href="blog.php">Blog</a></li>
                    <li class="<?php echo $active[3];?>"><a href="contacts.php">Contact</a></li>
                </ul>
            </nav>
        </header>