<?php
	$page_title = 'Home';
	$active = array("active","","","");
	include('templates/header.php');
?>
	<div class="slider">
		<div class="clearfix">
			<div class="slider_item sl_item_1">
				<a href="javascript:void(0)" class="slider_thumb ">
					<div class="slider_thumb_hover"></div>
					<img src="img/slider_images/sl_img_1.jpg" width="307" height="74" alt="" />
				</a>
			</div>
			<div class="slider_item sl_item_2">
				<a href="javascript:void(0)" class="slider_thumb">
					<div class="slider_thumb_hover"></div>
					<img src="img/slider_images/sl_img_2.jpg" width="315" height="74" alt="" />
				</a>
			</div>
			<div class="slider_item sl_item_3 clear_margin_right">
				<a href="javascript:void(0)" class="slider_thumb">
					<div class="slider_thumb_hover"></div>
					<img src="img/slider_images/sl_img_3.jpg" width="307" height="74" alt="" />
				</a>
			</div>
			<div class="slider_item sl_item_4">
				<a href="javascript:void(0)" class="slider_thumb">
					<img src="img/slider_images/sl_img_4.jpg" width="627" height="299" alt="" />
				</a>
				<div class="top_post_summary">
					<h2>
						<a href="javascript:void(0)">Top iPhone Apps</a>
					</h2>
					<div>
						Lorem ipsum dolor sit amet, te possim inimicus ius. Alii ullam at corper pri ad, per nulla luptatum te, in qui delenit nostrum. Nam ad labores.
					</div>
					<a class="read_more" href="javascript:void(0)">More</a>
				</div>
			</div>
			<div class="slider_item sl_item_5 clear_margin_right">
				<a href="javascript:void(0)" class="slider_thumb">
					<div class="slider_thumb_hover"></div>
					<img src="img/slider_images/sl_img_5.jpg" width="307" height="183" alt="" />
				</a>
			</div>
			<div class="slider_item sl_item_6 clear_margin_right">
				<a href="javascript:void(0)" class="slider_thumb">
					<div class="slider_thumb_hover"></div>
					<img src="img/slider_images/sl_img_6.jpg" width="307" height="110" alt="" />
				</a>
			</div>
			<div class="slider_item sl_item_7">
				<a href="javascript:void(0)" class="slider_thumb">
					<div class="slider_thumb_hover"></div>
					<img src="img/slider_images/sl_img_7.jpg" width="307" height="116" alt="" />
				</a>
			</div>
			<div class="slider_item sl_item_8">
				<a href="javascript:void(0)" class="slider_thumb">
					<div class="slider_thumb_hover"></div>
					<img src="img/slider_images/sl_img_8.jpg" width="315" height="116" alt="" />
				</a>
			</div>
			<div class="slider_item sl_item_9 clear_margin_right">
				<a href="post1.php" class="slider_thumb">
					<div class="slider_thumb_hover"></div>
					<img src="img/slider_images/sl_img_9.jpg" width="307" height="116" alt="" />
				</a>
			</div>
		</div>
		<a href="javascript:void(0)" class="slider_prev" title="previous"></a>
		<a href="javascript:void(0)" class="slider_next" title="next"></a>
	</div>
	<section id="home_wrapper">
		<h1>A Theme Unlike Any Other. Simply Fantastic!</h1>
		<?php include('templates/blog_posts.php') ?>
	</section>
	<nav class="pagination clearfix">
		<a href="javascript:void(0)" class="">1</a>
		<a href="javascript:void(0)" class="current">2</a>
		<a href="javascript:void(0)" class="">3</a>
		<a href="javascript:void(0)" class="">4</a>
	</nav>
	<?php include('templates/footer.php') ?>